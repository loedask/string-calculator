# String Calculator Documentation

Technical Interview - String Calculator

------

## Requirements

String Calculator is a console app that was built using [Laravel Zero](https://laravel-zero.com/), this means that the same system requirements needed for the script are the same as the system requirements for [Laravel 8.8](https://laravel.com/docs/8.x/installation).

 - PHP >= 7.3

## Getting Started

#### Clone the repository
1. From the repository in the global sidebar, select Clone.
2. Copy the clone command (either the SSH format or the HTTPS)
If you are using the SSH protocol, ensure your public key is in Bitbucket and loaded on the local system to which you are cloning.
3. From a terminal window, change into the local directory where you want to clone your repository.
4. Paste the command you copied from Bitbucket.

Clone over HTTPS
```
git clone https://loedask@bitbucket.org/loedask/string-calculator.git
```

Clone over SSH
```
git clone git@bitbucket.org:loedask/string-calculator.git
```

If the clone was successful, a new sub-directory appears on your local drive with the same name as the repository that you cloned.

## Run the String Calculator

1. Change to the local directory of the Bitbucket repository that you have just cloned ```cd /path/to/your-local/directory```
2. Run composer install
3. Run command ```php sc run``` to run the application
4. You can now use/test the application



## DIRECTORY STRUCTURE

```
--------------------------------------------------------------------------------
String Calculator
--------------------------------------------------------------------------------
    app/                    The app directory holds the base code for the String Calculator.
        Commands/           Contains commands classes used to run the String Calculator 

        Exceptions/         Contains exceptions used by the String Calculator 

        Services            Contains classes that are used by the String Calculator (All my Technical Interview codes)

    bootstrap               The bootstrap directory holds all the bootstrapping scripts used for the application.
    config                  The config directory holds all the project configuration files (.config).
    test	                The test directory holds all the test cases.
    vendor	                The vendor directory holds all composer dependency files.

```

## Additional Commands
1. Run the String Calculator - the application ```php sc run```
1. Run the tests ```./vendor/bin/pest```

## Contact

If you have any question, please contact me by sending an e-mail to Daskana Loecos via [loedask@hotmail.com](mailto:loedask@hotmail.com).

<?php

namespace App\Commands;

use App\Services\StringCalculator;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class RunCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'run';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Run the Application';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(StringCalculator $runApp)
    {
        $numbers = $this->ask('Please enter your numbers');
        $this->info($runApp->add($numbers));

    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}

<?php

namespace App\Services;

/**
 * Application Name and Version
 */
class Application
{

    /**
     * Application version.
     *
     * @var string
     */
    const VERSION = 'v1.7';

    /**
     * Application version.
     *
     * @var string
     */
    const NAME = 'String Calculator';


    /**
     * Get the version number of the application.
     *
     * @return string
     */
    public function version() : string
    {
        return static::VERSION;
    }


    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function name() : string
    {
        return static::NAME;
    }

}

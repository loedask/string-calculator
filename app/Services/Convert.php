<?php

namespace App\Services;

/**
 * Convert Class
 *
 * Convert string to an array
 *
 * @param  string $string
 */
class Convert
{
    protected $string;


    public function __construct($string)
    {
        $this->string = $string;
    }


    /**
     * stringToArray Method
     *
     * Convert @param $numbers to an array
     *
     * @return array
     */
    public function stringToArray(): array
    {
        preg_match_all('/\d+/', $this->string, $find);
        return $this->removeLargerNumbers($find[0]);
    }

    /**
     * removeLargerNumbers Method
     *
     * Remove numbers that are larger than 1000 from @param $numbers
     *
     * @param  array $numbers
     * @return array
     */
    protected function removeLargerNumbers($numbers = []): array
    {
        foreach ($numbers as $key => $value) {
            if ($value > 1000) {
                unset($numbers[$key]);
            }
        }

        return $numbers;
    }
}

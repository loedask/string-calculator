<?php

namespace App\Services;

use App\Exceptions\StringCalculatorException;
use App\Services\Detect;

/**
 * String Calculator
 */
class StringCalculator
{
    /**
     * Add Method
     *
     * Get the String Numbers inserted by the user.
     *
     * @param  string $numbers
     *
     */
    public function Add(string $numbers = null)
    {
        try {
            $detect = (new Detect($numbers));

            if ($detect->ifEmpty()) {
                return 0;
            }

            if ($detect->ifNegative()) {
                throw new StringCalculatorException($detect->allNegativeNumbers());
            }

            return (new Calculate($numbers))->sum();

        } catch (StringCalculatorException $e) {
            return $e->getMessage();
        }
    }
}

<?php

namespace App\Services;

/**
 * Detect Class
 *
 * @param  string $numbers
 */
class Detect
{

    /**
     * numbers
     *
     * @var string
     */
    protected $numbers;


    public function __construct($numbers)
    {
        $this->numbers = $numbers;
    }


    private function getNumbersArray(): array
    {
        return (new Convert($this->numbers))->stringToArray();
    }

    /**
     * ifNegative Method
     *
     * Check if array is null
     *
     * @return bool
     */
    public function ifEmpty(): bool
    {
        if (empty($this->getNumbersArray())) {
            return true;
        }
        return false;
    }



    /**
     * ifNegative Method
     *
     * Check if array has a negative sign
     *
     * @return bool
     */
    public function ifNegative(): bool
    {
        $split = str_split($this->numbers);

        if (in_array("-", $split)) {
            return true;
        }

        return false;
    }


    /**
     * allNegativeNumbers Method
     *
     * Get list of negative numbers from array
     *
     * @return string
     */
    public function allNegativeNumbers(): string
    {
        $negative_numbers =  array_filter($this->getNumbersArray(), function ($x) {
            return $x < 0;
        });

        return "Negatives not allowed" . implode(",", $negative_numbers);
    }
}

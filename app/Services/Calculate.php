<?php

namespace App\Services;

/**
 * Calculate Class
 *
 * @param  string $numbers
 */
class Calculate
{

    /**
     * numbers
     *
     * @var string
     */
    protected $numbers;


    public function __construct($numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * sum Method
     *
     * Calculate the sum of values in a given array
     *
     * @return int
     */
    public function sum(): int
    {
        $converted_string = (new Convert($this->numbers))->stringToArray();
        return array_sum($converted_string);
    }
}

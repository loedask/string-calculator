<?php

use App\Services\StringCalculator;
use Tests\TestCase;

class StringCalculatorTest extends TestCase
{
    public function testFailureOnEmptyString(): void
    {
        $this->assertEmpty(['foo']);
    }
}

<?php

use App\Services\Convert;
use Tests\TestCase;

class ConvertTest extends TestCase
{
    protected $string;
    

    public function testStringToArray()
    {
        $this->assertIsString('String Calculator');
    }

    protected function testRemoveLargerNumbers($numbers = []): array
    {
        foreach ($numbers as $key => $value) {
            if ($value > 1000) {
                unset($numbers[$key]);
            }
        }

        return $numbers;
    }
}

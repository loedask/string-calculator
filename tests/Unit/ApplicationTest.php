<?php

use App\Services\Application;
use Tests\TestCase;

class ApplicationTest extends TestCase
{
    public function testVersion()
    {
        $this->assertIsString('v1.3.1');
    }

    public function testName()
    {
        $this->assertIsString('String Calculator');
    }
}
